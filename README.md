
# Arviem

## Live demo
<https://arviem-demo.firebaseapp.com>


## Install

Run `npm install` 

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 

## Integration test

Run `npm run e2e` to launch cypress and run "locations-create" e2e test.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## CLI
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.
