context('locations-create', () => {
    beforeEach(() => {
    })

    it('Click on "Create Location" button', () => {

        cy.visit('https://arviem-demo.firebaseapp.com/')

        // click on "Create Location"
        cy.get('.mat-primary > .mat-button-wrapper').click()

        cy.location().should((location) => {
            expect(location.href).to.eq('https://arviem-demo.firebaseapp.com/locations/create')
        })
    })

    it('Fill form and save it', () => {

        // type data to form
        cy.get('#mat-input-2').type('testName')
        cy.get('#mat-input-3').type('testname')
        cy.get('#mat-input-4').type('48.1486')
        cy.get('#mat-input-5').type('17.1077')
        cy.get('#mat-input-6').type('Street')
        cy.get('#mat-input-7').type('City')
        cy.get('#mat-input-8').type('CH')

        // select "EUROPE"
        cy.get('#mat-select-2 > .mat-select-trigger > .mat-select-arrow-wrapper > .mat-select-arrow').click()
        cy.get('#mat-option-12 > .mat-option-text').click()

        // select "RAIL_TERMINAL"
        cy.get('#mat-select-3 > .mat-select-trigger > .mat-select-arrow-wrapper > .mat-select-arrow').click()
        cy.get('#mat-option-17 > .mat-option-text').click()

        // create locations
        cy.get('.button-save').click()

        // assert
        cy.get('tbody > :nth-child(1) > .cdk-column-name')
        cy.get('tbody > :nth-child(1) > .cdk-column-name').should('have.text', ' testName ')
    })

})
