import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationsTableFilterComponent } from './locations-table-filter.component';

describe('LocationsTableFilterComponent', () => {
  let component: LocationsTableFilterComponent;
  let fixture: ComponentFixture<LocationsTableFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationsTableFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsTableFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
