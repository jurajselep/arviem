import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms'
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-locations-table-filter',
  templateUrl: './locations-table-filter.component.html',
  styleUrls: ['./locations-table-filter.component.css']
})
export class LocationsTableFilterComponent implements OnInit, OnDestroy {

  public locationsTableFilter
  public onDestroy$ = new Subject()

  // TODO: add types 
  constructor(
    public store: Store<any>,
    public fb: FormBuilder,
  ) { }

  ngOnInit() {

    // create form group
    this.locationsTableFilter = this.fb.group({
      id: new FormControl('', {
        validators: [
          Validators.min(0),
          Validators.pattern("^[0-9]*$"),
        ],
        updateOn: 'blur'
      }),
      function: new FormControl('', {
        updateOn: 'blur'
      }),
      name: new FormControl('', {
        validators: [
          Validators.minLength(0),
          Validators.maxLength(60),
        ],
        updateOn: 'blur'
      }),
    })

  }

  ngOnDestroy() {

    // close all open observables
    this.onDestroy$.next();
    this.onDestroy$.complete();

    // destroy location table component
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_FILTER_DESTROY',
    })

  }

  filterEvent() {

    // mark input 
    this.locationsTableFilter.controls.id.markAsTouched()
    this.locationsTableFilter.controls.function.markAsTouched()
    this.locationsTableFilter.controls.name.markAsTouched()

    // check validity
    this.locationsTableFilter.updateValueAndValidity()

    this.store.dispatch({
      type: 'LOCATIONS_TABLE_FILTER',
    })

  }

}
