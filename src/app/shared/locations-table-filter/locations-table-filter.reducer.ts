
const initialState: any = {
    form: {
        id: '',
        name: '',
        function: '',
    },
    query: '',
}

export function reducer(state = initialState, action) {
    switch (action.type) {

        case 'LOCATIONS_TABLE_FILTER':
            return {
                ...state,
                // createa filter query
                query:
                    (state.form.id ? '&id=' + state.form.id : '') +
                    (state.form.function ? '&function=' + state.form.function : '') +
                    (state.form.name ? '&name=' + state.form.name : '')
            }

        default:
            return state;
    }
}