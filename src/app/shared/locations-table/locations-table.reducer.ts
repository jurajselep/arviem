// TODO: add type
const initialState: any = {
    ids: [],
    entities: {},
    paginator: {
        pageIndex: 0,
        pageSize: 5,
        pageSizeOptions: [5, 10, 25, 50, 100],
        length: 10,
    },
    sort: {
        order: [],
        query: '',
    }
}

export function reducer(state = initialState, action) {
    switch (action.type) {

        // transform table response data
        case 'LOCATIONS_TABLE_LOAD_SUCCESS': {

            return {
                ...state,
                ids: [
                    ...action.payload.map(location => location.resourceId)
                ],
                entities: {
                    ...action.payload.reduce((accumulator, location) => {
                        accumulator[location.resourceId] = location;
                        return accumulator;
                    }, {})
                },
                paginator: {
                    ...state.paginator,
                    // if we have last page, disable next page arrow  
                    length: action.payload.length < state.paginator.pageSize ?
                        (state.paginator.pageIndex * state.paginator.pageSize) :
                        state.paginator.length
                }
            };
        }

        // set paginator state 
        case 'LOCATIONS_TABLE_PAGINATOR': {
            console.log('[LOCATIONS_TABLE_PAGINATOR]', state )
            return {
                ...state,
                paginator: {
                    ...state.paginator,
                    ...action.payload,
                    length: (action.payload.pageIndex + 2) * action.payload.pageSize
                }
            }
        }

        // set sort state 
        case 'LOCATIONS_FORM_CREATE_SUCCESS':
        case 'LOCATIONS_TABLE_SORT': {

            // remove column if it exist already
            let orderedSort = state.sort.order
                .filter(item => item.active !== action.payload.active)

            // add element to array if it has direction
            if (action.payload.direction) orderedSort.push(action.payload)

            return {
                ...state,
                sort: {
                    order: orderedSort,
                    // transform to string supported by API
                    query: orderedSort.reverse().reduce((accumulator, item) => {
                        // add sort colums to query
                        return accumulator +
                            // add , if there is more than one element    
                            (accumulator ? ',' : '') +
                            // add - if desc direction
                            (item.direction === "desc" ? '-' + item.active : item.active)
                    }, '')
                }
            }
        }

        default:
            return state;
    }
}