import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-locations-table',
  templateUrl: './locations-table.component.html',
  styleUrls: ['./locations-table.component.css']
})
export class LocationsTableComponent implements OnInit, OnDestroy {

  public onDestroy$ = new Subject()

  // TODO: add types 
  public tableData
  public tableDataSource
  public tablePaginator
  public tableColumns: string[] = ['id', 'function', 'name', 'actions'];

  // TODO: add types 
  constructor(
    public store: Store<any>,
  ) { }

  ngOnInit() {

    // wait for data changes from redux    
    this.store.select('locations','locationsTable')
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(data => {
        // TODO: add types
        this.tableData = data.ids.map(id => ({ id, ...data.entities[id] }))
        this.tableDataSource = new MatTableDataSource<any>(this.tableData);
        this.tablePaginator = data.paginator;
      })

    // dispatch table pagination, load event
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_PAGINATOR',
      payload: {
        pageIndex: 0,
        pageSize: 5
      }
    })

  }

  ngOnDestroy() {

    // close all open observables
    this.onDestroy$.next();
    this.onDestroy$.complete();

    // destroy location table component
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_DESTROY',
    })

  }

  paginatorEvent(event) {

    // dispatch pagination event
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_PAGINATOR',
      payload: event
    })

  }

  sortEvent(event) {

    // dispatch sort event
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_SORT',
      payload: event
    })

  }

  updateEvent(event) {

    // dispatch sort event
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_UPDATE',
      payload: event
    })

  }
}
