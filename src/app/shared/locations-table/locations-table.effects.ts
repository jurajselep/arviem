
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { map, withLatestFrom, switchMap, catchError, tap } from 'rxjs/operators';

@Injectable()

export class LocationsTableEffects {

    @Effect()
    LocationsTableLoad$ = this.actions$.pipe(
        ofType('LOCATIONS_TABLE_LOAD'),

        // get state from store
        withLatestFrom(this.store, (action, state: any) => state),

        // get data from API endpoint 
        switchMap(state =>
            // we are using noCorsProxy to temporary bypass cors validation on server
            // for production setup CORS should be added on server 
            this.http.get(state.app.API.noCorsProxy + state.app.API.URL +
                '/tenant1/locations' +
                '?page=' + state.locations.locationsTable.paginator.pageIndex +
                '&pageSize=' + state.locations.locationsTable.paginator.pageSize +
                '&sort=' + state.locations.locationsTable.sort.query +
                state.locations.locationsTableFilter.query

            )
        ),

        // dispatch action with response data     
        map(response => ({ type: 'LOCATIONS_TABLE_LOAD_SUCCESS', payload: response })),

        // catch ERROR and resume ngrx effects stream
        catchError((error, caught) => {
            this.store.dispatch({ type: 'LOCATIONS_TABLE_LOAD_ERROR', payload: error.message });
            return caught;
        }),
    )

    // reload table data     
    @Effect()
    LocationsTableReload$ = this.actions$.pipe(
        ofType('LOCATIONS_TABLE_PAGINATOR', 'LOCATIONS_TABLE_SORT', 'LOCATIONS_TABLE_FILTER'),
        map(() => ({ type: 'LOCATIONS_TABLE_LOAD' })),
    )

    // redirect to update page 
    @Effect({ dispatch: false })
    LocationsTableUpdateRedirect$ = this.actions$.pipe(
        ofType('LOCATIONS_TABLE_UPDATE'),
        tap(() => this.router.navigate(['/locations/update']))
    )

    // redirect to update page 
    @Effect({ dispatch: false })
    LocationsTableCreateRedirect$ = this.actions$.pipe(
        ofType('LOCATIONS_TABLE_CREATE'),
        tap(() => this.router.navigate(['/locations/create']))
    )



    constructor(
        private actions$: Actions,
        private store: Store<any>,
        private http: HttpClient,
        private router: Router,
    ) { }

}
