import { Directive, Input, OnInit, OnDestroy } from '@angular/core';
import { FormGroupDirective } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[ngrxForm]'
})
export class NgrxFormDirective implements OnInit, OnDestroy {

  // use path for meta reducer to locate right place to upade 
  @Input('ngrxForm') path: string;

  private destroy$ = new Subject<null>();
  private updating = false;
  private form = {};

  constructor(
    private store: Store<any>,
    private formGroupDirective: FormGroupDirective,
  ) { }

  ngOnInit() {

    // listen & dispatch action when input value changes  
    this.formGroupDirective.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.updating = true;

        this.store.dispatch({
          type: "FORM_VALUE_CHANGES",
          payload: {
            path: this.path,
            value: value,
            // dirty: this.formGroupDirective.dirty,
            // errors: this.formGroupDirective.errors,
          }

        })

      })

    // listen to changes from redux and update form
    this.store.select('locations', this.path)
      .pipe(takeUntil(this.destroy$))
      .subscribe(state => {
        console.log('[directive][ngrx-form]', state, this.path)
        // update form form with redux data
        if (state) {
          // console.log('[ngrx-from] state:  ', state.form)
          this.formGroupDirective.form.patchValue({ ...state.form }, { emitEvent: false });
        }

      })
  }



  ngOnDestroy() {

    // close all open directives
    this.destroy$.next();
    this.destroy$.complete();

    //TODO: clean and destroy form

  }

}
