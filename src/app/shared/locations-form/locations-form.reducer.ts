
const initialState: any = {
    form: {
        name: '',
        normalizedName: '',
        coordinates: {
            latitudeInDegrees: '',
            longitudeInDegrees: ''
        },
        address: {
            city: '',
            country: '',
            continent: '',
            streetName: '',
            streetNumber: '',
            postalCode: '',
            administrativeLevel2: '',
            administrativeLevel1: ''
        },
        function: ''
    },
}

export function reducer(state = initialState, action) {
    switch (action.type) {

        case 'LOCATIONS_TABLE_CREATE':
        case 'LOCATIONS_FORM_CREATE_ERROR':
        case 'LOCATIONS_FORM_CREATE_SUCCESS':
        case 'LOCATIONS_FORM_UPDATE_SUCCESS':
        case 'LOCATIONS_FORM_UPDATE_ERROR':
            return initialState;

        case 'LOCATIONS_TABLE_UPDATE':
            return {
                form: {
                    ...action.payload,
                }
            }

        // increase version    
        case 'LOCATIONS_FORM_UPDATE': {
            let cleanedForm = {
                ...state.form,
            }

            //remove id property
            delete cleanedForm['id'];
            
            return {
                form: cleanedForm,
            }
        }

        default:
            return state;
    }
}