
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

import { map, withLatestFrom, switchMap, catchError, tap } from 'rxjs/operators';

@Injectable()

export class LocationsFormEffects {

    @Effect()
    LocationsFormSave$ = this.actions$.pipe(
        ofType('LOCATIONS_FORM_CREATE'),

        // get state from store
        withLatestFrom(this.store, (action, state: any) => state),

        // get data from API endpoint 
        switchMap(state =>
            // we are using noCorsProxy to temporary bypass cors validation on server
            // for production setup CORS should be added on server 
            this.http.post(state.app.API.noCorsProxy + state.app.API.URL + '/tenant1/locations',
                state.locations.locationsForm.form
            )
        ),

        // dispatch action with response data     
        map(response => ({ type: 'LOCATIONS_FORM_CREATE_SUCCESS', payload: { active: 'id', direction: 'desc' } })),

        // catch ERROR and resume ngrx effects stream
        catchError((error, caught) => {
            this.store.dispatch({ type: 'LOCATIONS_FORM_CTEATE_ERROR', payload: error.message });
            return caught;
        }),
    )

    @Effect()
    LocationsFormUpdate$ = this.actions$.pipe(
        ofType('LOCATIONS_FORM_UPDATE'),

        // get state from store
        withLatestFrom(this.store, (action, state: any) => state),

        // get data from API endpoint 
        switchMap(state =>
            // we are using noCorsProxy to temporary bypass cors validation on server
            // for production setup CORS should be added on server 
            this.http.put(state.app.API.noCorsProxy + state.app.API.URL + state.locations.locationsForm.form.resourceId,
                state.locations.locationsForm.form
            )
        ),

        // dispatch action    
        map(() => ({ type: 'LOCATIONS_FORM_UPDATE_SUCCESS' })),

        // catch ERROR and resume ngrx effects stream
        catchError((error, caught) => {
            this.store.dispatch({ type: 'LOCATIONS_FORM_UPDATE_ERROR', payload: error.message });
            return caught;
        }),
    )

    @Effect({ dispatch: false })
    LocationsFormRedirect$ = this.actions$.pipe(
        ofType('LOCATIONS_FORM_CREATE_SUCCESS', 'LOCATIONS_FORM_UPDATE_SUCCESS'),
        tap(() => this.router.navigate(['/locations']))
    )

    constructor(
        private actions$: Actions,
        private store: Store<any>,
        private http: HttpClient,
        private router: Router,
    ) { }

}
