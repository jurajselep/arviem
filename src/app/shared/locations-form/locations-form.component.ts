import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms'
import { Store } from '@ngrx/store';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-locations-form',
  templateUrl: './locations-form.component.html',
  styleUrls: ['./locations-form.component.css']
})
export class LocationsFormComponent implements OnInit, OnDestroy {

  public coordinatesForm
  public addressForm
  public locationsForm
  public locationsFormData
  public onDestroy$ = new Subject()

  // TODO: add types 
  constructor(
    public store: Store<any>,
    public fb: FormBuilder,
  ) { }

  ngOnInit() {

    this.store.select('locations', 'locationsForm')
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(data => {
        console.log('[locastion-form][ngOnInit]', data)
        this.locationsFormData = data
      })

    // create coordinates form group
    this.coordinatesForm = this.fb.group({
      latitudeInDegrees: new FormControl('', {
        validators: [
          Validators.required,
          Validators.min(-90),
          Validators.max(90),
        ],
        updateOn: 'blur'
      }),
      longitudeInDegrees: new FormControl('', {
        validators: [
          Validators.required,
          Validators.min(-180),
          Validators.max(180),
        ],
        updateOn: 'blur'
      }),

    });

    // create coordinates form group
    this.addressForm = this.fb.group({
      streetName: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(60),
        ],
        updateOn: 'blur'
      }),
      city: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(60),
        ],
        updateOn: 'blur'
      }),
      country: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(2),
          // Validators.pattern("^[A-Z]*$"),
        ],
        updateOn: 'blur'
      }),
      continent: new FormControl('', {
        validators: [
          Validators.required,
        ],
        updateOn: 'blur'
      }),
    });

    // create location form group
    this.locationsForm = this.fb.group({
      name: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(60),
        ],
        updateOn: 'blur'
      }),
      normalizedName: new FormControl('', {
        validators: [
          Validators.required,
          Validators.minLength(1),
          Validators.maxLength(60),
          // only lower case no diacritics 
          Validators.pattern("^[0-9a-z]*$"),
        ],
        updateOn: 'blur'
      }),
      coordinates: this.coordinatesForm,
      address: this.addressForm,
      function: new FormControl('', {
        validators: [
          Validators.required
        ],
        updateOn: 'blur'
      }),
    })

  }

  ngOnDestroy() {

    // close all open observables
    this.onDestroy$.next();
    this.onDestroy$.complete();

    // destroy location table component
    this.store.dispatch({
      type: 'LOCATIONS_FORM_DESTROY',
    })

  }

  validateForm() {

    // mark input 
    this.locationsForm.controls.name.markAsTouched()
    this.locationsForm.controls.normalizedName.markAsTouched()

    this.locationsForm.controls.coordinates.controls.latitudeInDegrees.markAsTouched()
    this.locationsForm.controls.coordinates.controls.longitudeInDegrees.markAsTouched()

    this.locationsForm.controls.address.controls.streetName.markAsTouched()
    this.locationsForm.controls.address.controls.city.markAsTouched()
    this.locationsForm.controls.address.controls.country.markAsTouched()
    this.locationsForm.controls.address.controls.continent.markAsTouched()

    this.locationsForm.controls.function.markAsTouched()

    // check validity
    this.locationsForm.updateValueAndValidity()

  }

  saveEvent() {

    // check if we have valid form
    this.validateForm()

    // dispatch only if valid
    if (this.locationsForm.valid) {

      this.store.dispatch({
        type: 'LOCATIONS_FORM_CREATE',
      })

    }

  }

  updateEvent() {

    // check if we have valid form
    this.validateForm()

    // dispatch only if valid
    if (this.locationsForm.valid) {

      this.store.dispatch({
        type: 'LOCATIONS_FORM_UPDATE',
      })

    }

  }


}
