import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationsListComponent } from './locations/locations-list/locations-list.component';
import { LocationsCreateComponent } from './locations/locations-create/locations-create.component';
import { LocationsUpdateComponent } from './locations/locations-update/locations-update.component';


const routes: Routes = [

  { path: 'locations', component: LocationsListComponent },
  { path: 'locations/create', component: LocationsCreateComponent },
  { path: 'locations/update', component: LocationsUpdateComponent },
  
  // redirect to location
  { path: '', redirectTo: '/locations', pathMatch: 'full' },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
