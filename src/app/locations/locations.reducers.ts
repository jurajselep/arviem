import {
    ActionReducerMap,
  } from '@ngrx/store';
  

  // reducers 
  import * as fromLocationsForm from '../shared/locations-form/locations-form.reducer';
  import * as fromLocationsTable from '../shared/locations-table/locations-table.reducer';
  import * as fromLocationsTableFilter from '../shared/locations-table-filter/locations-table-filter.reducer';
  
  
  export interface State {
    // TODO: add type once it is clear 
    locationsForm: any;
    locationsTable: any;
    locationsTableFilter: any;
  }
  
  export const reducers: ActionReducerMap<State> = {
    locationsForm: fromLocationsForm.reducer,
    locationsTable: fromLocationsTable.reducer,
    locationsTableFilter: fromLocationsTableFilter.reducer,
  };
  
  
