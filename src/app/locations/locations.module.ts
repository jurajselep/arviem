import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { LocationsTableComponent } from '../shared/locations-table/locations-table.component';
import { LocationsFormComponent } from '../shared/locations-form/locations-form.component';
import { LocationsListComponent } from '../locations/locations-list/locations-list.component';
import { LocationsCreateComponent } from '../locations/locations-create/locations-create.component';
import { LocationsUpdateComponent } from '../locations/locations-update/locations-update.component';

import { LocationsTableFilterComponent } from '../shared/locations-table-filter/locations-table-filter.component';

import { NgrxFormDirective } from '../shared/ngrx-form.directive';

import { reducers } from './locations.reducers';

import { LocationsTableEffects } from '../shared/locations-table/locations-table.effects';
import { LocationsFormEffects } from '../shared/locations-form/locations-form.effects';

  // import angular material modules
  import {
    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule
  } from '@angular/material';

@NgModule({
  declarations: [

    LocationsTableComponent,
    LocationsFormComponent,
    LocationsListComponent,
    LocationsCreateComponent,
    LocationsUpdateComponent,
    LocationsTableFilterComponent,
    NgrxFormDirective,

  ],
  imports: [
    CommonModule,

    ReactiveFormsModule,
    HttpClientModule,

    StoreModule.forFeature('locations', reducers),

    EffectsModule.forFeature([
      LocationsFormEffects,
      LocationsTableEffects
    ]),

    MatButtonModule,
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
  ]
})
export class LocationsModule { }
