import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  createSelector,
  MetaReducer
} from '@ngrx/store';

import { storeFreeze } from 'ngrx-store-freeze';
import { environment } from '../environments/environment';


// reducers 
import * as fromApp from './app.reducer';

// meta reducert for dynamic forms
import * as fromNgrxForm from './shared/ngrx-form.reducer';

export interface State {
  // TODO: add type once it is clear 
  app: any;
}

export const reducers: ActionReducerMap<State> = {
  app: fromApp.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production
  ? [fromNgrxForm.form, storeFreeze]
  : [fromNgrxForm.form];
