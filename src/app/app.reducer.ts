// TODO: add type
const initialState: any = {
    API: {
        // https://github.com/Rob--W/cors-anywhere
        noCorsProxy : 'https://cors-anywhere.herokuapp.com/',
        URL: 'http://arviem-api.us-east-1.elasticbeanstalk.com'
    }
}

export function reducer(state = initialState, action) {
    switch (action.type) {

        default:
            return state;
    }
}