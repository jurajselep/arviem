import { Component } from '@angular/core';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'arviem';

  constructor(
    public store: Store<any>,
  ) { }

  createLocationEvent() {

    // destroy location table component
    this.store.dispatch({
      type: 'LOCATIONS_TABLE_CREATE',
    })

  }

}
