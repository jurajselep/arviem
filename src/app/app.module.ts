import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';

import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LocationsModule } from "./locations/locations.module"

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';


import { AppEffects } from './app.effects';
import { reducers, metaReducers } from './app.reducers';

import { environment } from '../environments/environment';

// import angular material modules
import {
  MatButtonModule
} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,

    LocationsModule,

    StoreModule.forRoot(reducers, { metaReducers }),

    !environment.production ? StoreDevtoolsModule.instrument() : [],

    EffectsModule.forRoot([
      AppEffects
    ]),

  ],

  exports: [
  ],

  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
